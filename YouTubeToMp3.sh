#!/bin/bash

#######################################################################################################################################
#                                                                                                                                     #
# For more information about the way this script works, please advice file README.md                                                  #
#                                                                                                                                     #
#######################################################################################################################################

i=0
multChoice=0
choice=0
videoUrl=0
playlistUrl=0
path=0
dirCreated=0

#Check if prerequisites are installed
echo -e "Please ensure that you have installed libraries ffmpeg and youtube-dl before you move forward (0-exit, 1-continue)"
read choice

if [ $choice -eq 0 ]; then
   exit 1
fi

#Create required folders
cd ${HOME}
if [[ ! -e "YouTubeToMp3" ]]; then
   mkdir -p "YouTubeToMp3/Mp3s"
   mkdir -p "YouTubeToMp3/Videos"
   mkdir -p "YouTubeToMp3/temp"
   dirCreated=${HOME}/"YouTubeToMp3"
elif [[ ! -d "YouTubeToMp3" ]]; then
   mkdir -p "YouTubeToMp3_/Mp3s"
   mkdir -p "YouTubeToMp3_/Videos"
   mkdir -p "YouTubeToMp3_/temp"
   dirCreated=${HOME}/"YouTubeToMp3_"
else
   dirCreated=${HOME}/"YouTubeToMp3"
fi

#Collect required information from user
echo -e "Insert (1) for single song, or (2) for more (playlist, file):"
read choice

while [[ $choice -ne 1 && $choice -ne 2 ]]; do
   echo -e "Insert (1) for single song, or (2) for more (playlist, file):"
   read choice
done

if [ $choice -eq 1 ]; then
   echo -e "Insert video URL:"
   read videoUrl
elif [ $choice -eq 2 ]; then
   echo -e "Insert (1) for playlist, or (2) for URLs in separate file:"
   read multChoice
   if [ $multChoice -eq 1 ]; then
      echo -e "Insert playlist's URL:"
      read playlistUrl
   elif [ $multChoice -eq 2 ]; then
      echo -e "Insert absolut path to the file including the list of URLs (i.e. /home/user/Desktop/list.txt):"
      read path
   fi
fi

echo -e "Insert the desired bitrate of mp3s in Kbps (i.e. 320):"
read abitrate
abitrate+="k"

echo -e "Do you wish to remove video files after convertion? (1-yes, 0-no)"
read deletion

#Download videos
cd $dirCreated/"temp"
rm -rf *

if [ $choice -eq 1 ]; then
   youtube-dl --no-playlist --audio-quality 0 "$videoUrl"
elif [ $multChoice -eq 1 ]; then
   youtube-dl --yes-playlist --audio-quality 0 "$playlistUrl"
elif [ $multChoice -eq 2 ]; then
   if [[ -e "$path"  || -f "$path" ]]; then
      if [ -s "$path" ]; then
        cat $path | while read line; do
           youtube-dl --no-playlist --audio-quality 0 "$line"
           i=$((i+1));
        done
      else
        echo "Your file is empty! Terminating..."
        exit 1
      fi
   else
      echo "The path to the file is not correct! Terminating..."
      exit 1
   fi
fi

#Convert videos to mp3s and delete source files
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
cd $dirCreated/"Mp3s"
for j in $(ls "$dirCreated/temp"); do
   ffmpeg -i "$dirCreated/temp/$j" -b:a $abitrate -acodec mp3 "${j%.*}.mp3"
   i=$((i+1))
   if [ $deletion -eq 1 ]; then
      rm "$dirCreated/temp/$j"
   elif [ $deletion -eq 0 ]; then
      mv "$dirCreated/temp/$j" "$dirCreated/Videos/$j"
   fi
   continue;
done
IFS=$SAVEIFS

echo "You can find your song(s) under $dirCreated/Mp3s !"
