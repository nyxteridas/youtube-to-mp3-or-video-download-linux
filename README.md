## youtube-to-mp3-or-video-download-linux v1.0

Bash script for downloading youtube videos, playlists or list of videos and convert them optionally to mp3.

---

## Introduction

YouTubeToMp3 is a linux script(bash) able to help whoever wants to download songs from YouTube in a good quality, 
without the need to use any third-party website (which may not guarantee the sound quality)

**Prerequisites**

You should have installed in your system the libraries youtube-dl and ffmpeg.                                                           
You can use the below commands in order to do this:

1. sudo apt-get install youtube-dl                                                                                               
2. sudo apt-get install ffmpeg                                                                                                   

---

## How it works

**The script requires interaction with the user**
You are given the below options:

1. Download a single song
2. Download songs which are included in a youtube playlist
3. Download a set of songs, the URLs of which you have put in a txt file (one URL per line)

By default the script downloads the video at the best possible quality.
However, the bitrate of the final file, has to be defined by you.
You can also select if you want to delete the video files after the convertion or not.

---

## Where are the files stored

By default, the script creates a folder with the name "YouTubeToMp3" under your home folder.
Within this folder, three others are being created, one for the videos, one for the songs, and one for temporary files.
Every time the script starts, the folder "temp" is empty.

The folder hierarchy is shown below:

   |-/                                                                                                                            
    |-home                                                                                                                        
      |-<username>                                                                                                                
        |-YouTubeToMp3                                                                                                            
          |--Videos                                                                                                               
          |--Mp3s                                                                                                                 
          |--temp                                                                                                                 

If under your home folder, another directory with name "YouTubeToMp3" exists, another one is created with name "YouTubeToMp3_".